(function ($, Drupal) {
	Drupal.behaviors.VbMediaAdmin = {
		attach: function (context, settings) {
			// Automatically open entity browser modals so we don't have to click twice
			if($(context).is('[id^="inline-entity-form"]')) {
				$(context).find('input[id*="entity-browser-open-modal"]').trigger('click');
			}

			$('.entity-browser-form .views-field-rendered-entity').off('click.select').on('click.select', function(event) {
				event.preventDefault();
				event.stopPropagation();
				$(this).next('.views-field-entity-browser-select').find('.form-checkbox, .form-radio').trigger('click');
				
				if($(this).hasClass('selected')) {
					$(this).removeClass('selected');
					$(this).next('.views-field-entity-browser-select').find('.form-checkbox, .form-radio').prop('checked', false);
				} else {
					if($(this).next('.views-field-entity-browser-select').find('.form-radio').length) {
						$('.views-field-rendered-entity').removeClass('selected');
					}
					$(this).addClass('selected');
				}
			});
			$('.entity-browser-form .form-managed-file .option').off('click.option').on('click.option', function(event) {
				event.preventDefault();
			});
			$('.entity-browser-form .form-managed-file .cancel').off('click.cancel').on('click.cancel', function(event) {
				event.preventDefault();
				event.stopPropagation();

				$('.entity-browser-form .form-managed-file .form-checkbox').prop('checked', false);
				$(this).parent('.option').prev('.form-checkbox').prop('checked', true);
				$('.entity-browser-form .form-managed-file input[name="upload_remove_button"]').trigger('mousedown');
			});
		}
	};
})(jQuery, Drupal);