<?php

namespace Drupal\vb_media_pexels\Plugin\MediaLibrarySource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Drupal\media_library_extend\Plugin\MediaLibrarySource\MediaLibrarySourceBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media library pane to pull images from Pexels.
 *
 * @MediaLibrarySource(
 *   id = "pexels",
 *   label = @Translation("Pexels"),
 *   source_types = {
 *     "image",
 *   },
 * )
 */
class Pexels extends MediaLibrarySourceBase {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('file_system'),
      $container->get('http_client'),
      $container->get('file.repository')
    );
  }

  /**
   * Constructs a new Pexels object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file_system service.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Token $token,
    FileSystemInterface $file_system,
    Client $http_client,
    FileRepositoryInterface $file_repository
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $token, $file_system);
    $this->httpClient = $http_client;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#title' => $this->t('API Key'),
      '#description' => $this->t('To use this plugin you need a valid Pexels API key.'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getResults() {
    $keyword = $this->getValue('keyword') ?? 'dummy';

    $response = $this->httpClient->request('GET', 'https://api.pexels.com/v1/search', [
      'headers' => [
        'Authorization' => $this->configuration['api_key'],
      ],
      'query' => [
        'query' => $keyword, // @todo make searchable.
        'page' => $this->getValue('page') + 1,
        'limit' => $this->configuration['items_per_page'],
      ],
    ]);

    // @todo Error handling.
    $data = json_decode((string) $response->getBody(), TRUE);

    $results = [];

    if (isset($data['photos']) && !empty($data['photos'])) {
      foreach ($data['photos'] as $photo) {
        $results[] = [
          'id' => $photo['id'],
          'label' => $photo['photographer'],
          'preview' => [
            '#type' => 'html_tag',
            '#tag' => 'img',
            '#attributes' => [
              'src' => $photo['src']['tiny'],
              'alt' => $photo['alt'],
              'title' => $photo['alt'],
            ],
          ],
        ];
      }
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['keyword'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Keyword'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId($selected_id) {
    $response = $this->httpClient->request('GET', 'https://api.pexels.com/v1/photos/' . $selected_id, [
      'headers' => [
        'Authorization' => $this->configuration['api_key'],
      ],
    ]);

    // @todo Error handling.
    $info = json_decode((string) $response->getBody(), TRUE);

    $url = $info['src']['original'];

    // Create a media entity.
    $entity = $this->createEntityStub('Pexels - ' . $selected_id);

    // Download the requested file.
    try {
      $response = $this->httpClient->request('GET', $url, [
        'timeout' => 30,
      ]);

      if ($response->getStatusCode() != 200) {
        // @todo Error handling.
        return NULL;
      }

      // Get file extension from response, since the selected download profile
      // might use a different extension than the original file.
      $filename = $selected_id . '.jpg';

      // Save to filesystem.
      $file = $this->fileRepository->writeData($response->getBody(), $this->getUploadLocation() . '/' . $filename);

      // Attach file to media entity.
      $source_field = $this->getSourceField();
      $entity->{$source_field}->target_id = $file->id();
      $entity->{$source_field}->alt = $info['alt'];
      $entity->save();

      return $entity->id();
    }
    catch (TransferException $e) {
      $logger = \Drupal::logger('media_library_extend');
      Error::logException($logger, $e);
    }
  }

}
