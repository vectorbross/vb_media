require 'autoprefixer-rails'
require 'sass-globbing'


# Set this to the root of your project when deployed:
http_path = "../"
css_dir = "css"
sass_dir = "css"
images_dir = ""
javascripts_dir = ""
fonts_dir = ""

output_style = :compressed
line_comments = false

on_stylesheet_saved do |file|
  css = File.read(file)
  map = file + '.map'

  if File.exists? map
    result = AutoprefixerRails.process(css,
      from: file,
      to:   file,
      map:  { prev: File.read(map), inline: false })
    File.open(file, 'w') { |io| io << result.css }
    File.open(map,  'w') { |io| io << result.map }
  else
    File.open(file, 'w') { |io| io << AutoprefixerRails.process(css, browsers: ['last 3 versions']) }
  end
end